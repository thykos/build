import gulp from 'gulp';
import server from './gulp_tasks/server';
import clean from './gulp_tasks/clean';
import slim, { slimProd } from './gulp_tasks/slim';
import scripts, { scriptsProd } from './gulp_tasks/scripts';
import styles, { stylesProd } from './gulp_tasks/styles';
import images, { imagesProd } from './gulp_tasks/images';
import fonts, { fontsProd } from './gulp_tasks/fonts';
import {addDepScripts, addDepStyles, addDepScriptsProd, addDepStylesProd} from './gulp_tasks/dependencies';

gulp.task('start', ['slim', 'scripts', 'styles', 'images', 'fonts', 'addDepScripts', 'addDepStyles', 'server']);
gulp.task('prod', ['slimProd', 'scriptsProd', 'stylesProd', 'imagesProd', 'fontsProd', 'addDepScriptsProd', 'addDepStylesProd']);
