import gulp from 'gulp';
import browserSync from 'browser-sync';
import {Public} from './dist';

export default gulp.task('server', () =>
  browserSync
    .create()
    .init({
      server: {
        baseDir: Public,
        index: `./index.html`,
        routes: {
            '../bower_components': 'bower_components'
        }
      }
    })
);

export const reload = () => browserSync.reload({stream: true});
