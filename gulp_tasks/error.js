import notify from 'gulp-notify';

export default (title) => notify.onError((err) => ({title: title, message: err.message}))