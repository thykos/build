export const Src = './src/';
const Assets = `${Src}/assets/`;
export const Scripts = `${Assets}/js/**/*.js`;
export const Styles = `${Assets}/css/**/*.scss`;
export const Fonts = `${Assets}/fonts/**/*`;
export const Images = `${Assets}/images/**/*`;
export const Slims = `${Src}/pages/**/*.slim`;
export const SlimsComponents = `${Src}pages/components/`;
export const BowerDir = './bower_components';

export const Public = './production/';
const PublicAssets = `${Public}/assets/`;
export const PublicScripts = `${PublicAssets}/js/`;
export const PublicStyles = `${PublicAssets}/css/`;
export const PublicFonts = `${PublicAssets}/fonts/`;
export const PublicImages = `${PublicAssets}/images/`;